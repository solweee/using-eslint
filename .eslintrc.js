module.exports = {
  env: {
    browser: true,
    es2021: true,
  },
  extends: "airbnb-base",
  overrides: [],
  parserOptions: {
    ecmaVersion: "latest",
    sourceType: "module",
  },
  rules: {
    quotes: ["error", "double"],
    "max-classes-per-file": 0,
    "global-require": 0,
    "linebreak-style": [0, "error", "windows"],
    "import/no-extraneous-dependencies": ["error", { devDependencies: true }],
    "no-alert": 0,
    "import/extensions": 0,
    "class-methods-use-this": 0,
    "consistent-return": 0,
    "comma-dangle": ["error", "only-multiline"],
  },
};
